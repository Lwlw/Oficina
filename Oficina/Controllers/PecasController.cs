﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Oficina.Helpers;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class PecasController : Controller
    {
        private OficinaContext db = new OficinaContext();

        
        // GET: Pecas
        //[Authorize(Roles="View")]
        public ActionResult Index()
        {
            ViewBag.Contagem = db.Pecas.Count();
            return View(db.Pecas.ToList());
        }


        // GET: Pecas/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Peca peca = db.Pecas.Find(id);
            if (peca == null)
            {
                return HttpNotFound();
            }

            return View(peca);
        }

        // GET: Pecas/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pecas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPeca,Descricao,Preco,Imagem,PecaImagemURL")] Peca peca)
        {
            if (ModelState.IsValid)
            {
                db.Pecas.Add(peca);
                db.SaveChanges();

                if (peca.PecaImagemURL != null)
                {
                    var pasta = "~/Content/Images";
                    var ficheiro = string.Format("{0}.jpg", peca.IdPeca);

                    var resposta = FilesHelper.UploadImagem(peca.PecaImagemURL, pasta, ficheiro);

                    if (resposta)
                    {
                        var pic = string.Format("{0}/{1}", pasta, ficheiro);
                        peca.Imagem = pic;
                        db.Entry(peca).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(peca);
        }


        // GET: Pecas/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Peca peca = db.Pecas.Find(id);
            if (peca == null)
            {
                return HttpNotFound();
            }
            return View(peca);
        }

        // POST: Pecas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPeca,Descricao,Preco,Imagem,PecaImagemURL")] Peca peca)
        {
            if (ModelState.IsValid)
            {
                if (peca.PecaImagemURL != null)
                {
                    var pic = string.Empty;
                    var pasta = "~/Content/Images";
                    var ficheiro = string.Format("{0}.jpg", peca.IdPeca);

                    var response = FilesHelper.UploadImagem(peca.PecaImagemURL, pasta, ficheiro);

                    if (response)
                    {
                        pic = string.Format("{0}/{1}", pasta, ficheiro);
                        peca.Imagem = pic;

                    }
                }

                db.Entry(peca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peca);
        }


        // GET: Pecas/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Peca peca = db.Pecas.Find(id);
            if (peca == null)
            {
                return HttpNotFound();
            }
            return View(peca);
        }

        // POST: Pecas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Peca peca = db.Pecas.Find(id);
            db.Pecas.Remove(peca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
