﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oficina.Helpers;
using Oficina.Models;
using Oficina.ViewModels;

namespace Oficina.Controllers
{
    [Authorize(Users = "luis@mail.pt")]
    public class MarcacoesController : Controller
    {

        private OficinaContext db = new OficinaContext();

        // GET: Marcacoes
        public ActionResult NovaReparacao()
        {

            var reparacaoView = new ReparacaoViewModels();
            reparacaoView.Viatura = new Viatura();
            reparacaoView.Mecanico = new Mecanico();
            reparacaoView.Pecas = new List<PecaReparacao>();

            Session["reparacaoView"] = reparacaoView;


            ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

            ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");


            return View(reparacaoView);
        }



        [HttpPost]
        public ActionResult NovaReparacao(ReparacaoViewModels reparacaoView)
        {
            reparacaoView = Session["reparacaoView"] as ReparacaoViewModels;

            var idViatura = int.Parse(Request["IdViatura"]);
            var data = DateTime.Parse(Request["Data"]);
            var idMecanico = int.Parse(Request["IdMecanico"]);

            if (idViatura == 0)
            {
                ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                    OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                    OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                ViewBag.Error = "Deve selecionar o cliente";

                return View(reparacaoView);
            }

            if (idMecanico == 0)
            {
                ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                    OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                    OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                ViewBag.Error = "Deve selecionar o mecânico";

                return View(reparacaoView);
            }

            var viatura = db.Viaturas.Find(idViatura);

            if (viatura == null)
            {
                ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                    OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                    OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                ViewBag.Error = "O cliente não existe";

                return View(reparacaoView);
            }

            var mecanico = db.Viaturas.Find(idViatura);

            if (mecanico == null)
            {
                ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                    OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                    OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                ViewBag.Error = "O mecanico não existe";

                return View(reparacaoView);
            }

            if (reparacaoView.Pecas.Count == 0)
            {
                ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                    OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                    OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                ViewBag.Error = "Deve escolher as peças a encomendar";

                return View(reparacaoView);
            }

            int idReparacao = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var reparacao = new Reparacao
                    {
                        IdViatura = idViatura,
                        DataInicio = data,
                        Status = Status.Activa,
                        Descricao = "...",
                        IdMecanico = idMecanico
                    };

                    db.Reparacaos.Add(reparacao);
                    db.SaveChanges();

                    idReparacao = reparacao.IdReparacao;


                    foreach (var item in reparacaoView.Pecas)
                    {
                        var reparacaoDetalhe = new ReparacaoDetalhe
                        {
                            IdReparacao = idReparacao,
                            IdPeca = item.IdPeca,
                            Quantidade = item.Quantidade
                        };

                        db.ReparacaoDetalhes.Add(reparacaoDetalhe);
                        db.SaveChanges();

                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    ViewBag.Error = $"Erro: {ex}";

                    ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                        OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

                    ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                        OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");

                    return View(reparacaoView);
                }
            }



            ViewBag.Message = $"A reparação nº. {idReparacao} foi marcada com sucesso!";

            ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

            ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");


            reparacaoView = new ReparacaoViewModels();
            reparacaoView.Viatura = new Viatura();
            reparacaoView.Pecas = new List<PecaReparacao>();

            Session["reparacaoView"] = reparacaoView;

            return View(reparacaoView);
            //return RedirectToAction("NovaReparacao");
        }




        public ActionResult AdicionaPeca()
        {

            ViewBag.IdPeca = new SelectList(CombosHelper.GetPecas(), "IdPeca", "Descricao");

            return View();
        }


        [HttpPost]
        public ActionResult AdicionaPeca(PecaReparacao pecaReparacao) 
        {
            var reparacaoView = Session["reparacaoView"] as ReparacaoViewModels;


            var idPeca = int.Parse(Request["IdPeca"]);

            if (idPeca == 0)
            {
                ViewBag.IdPeca = new SelectList(CombosHelper.GetPecas(), "IdPeca", "Descricao");
                ViewBag.Error = "Deve selecionar uma peça";

                return View(pecaReparacao);
            }


            var quantidade = Request["Quantidade"];

            if ( string.IsNullOrEmpty(quantidade))
            {
                ViewBag.IdPeca = new SelectList(CombosHelper.GetPecas(), "IdPeca", "Descricao");
                return View(pecaReparacao);
            }
            if (int.Parse(quantidade) <= 0)
            {
                ViewBag.IdPeca = new SelectList(CombosHelper.GetPecas(), "IdPeca", "Descricao");
                //ViewBag.Error = "Deve inserir uma quantidade superior a zero";

                return View(pecaReparacao);
            }


            var peca = db.Pecas.Find(idPeca);

            if (peca == null)
            {
                ViewBag.IdPeca = new SelectList(CombosHelper.GetPecas(), "IdPeca", "Descricao");
                ViewBag.Error = "A peça não existe";

                return View(pecaReparacao);
            }

            pecaReparacao = reparacaoView.Pecas.Find(p => p.IdPeca == idPeca);

            if (pecaReparacao == null)
            {
                pecaReparacao = new PecaReparacao
                {
                    Descricao = peca.Descricao,
                    Preco = peca.Preco,
                    IdPeca = peca.IdPeca,
                    Quantidade = int.Parse(Request["Quantidade"])

                };

                reparacaoView.Pecas.Add(pecaReparacao);
            }
            else
            {
                pecaReparacao.Quantidade += int.Parse(Request["Quantidade"]);
            }



            ViewBag.IdViatura = new SelectList(CombosHelper.GetViaturas().
                OrderBy(x => x.ClienteNome), "IdViatura", "ClienteNome");

            ViewBag.IdMecanico = new SelectList(CombosHelper.GetMecanicos().
                OrderBy(x => x.NomeMecanico), "IdMecanico", "NomeMecanico");


            return View("NovaReparacao", reparacaoView);

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}