﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Oficina.Helpers;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class ClientesController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Clientes
        //[Authorize(Roles = "View")]
        public ActionResult Index()
        {
            ViewBag.Contagem = db.Clientes.Count();
            return View(db.Clientes.ToList());
        }

        // GET: Clientes/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            ViewBag.CodPostal = new SelectList(CombosHelper.GetCodigosPostais().OrderBy(m => m.cod_postal), "cod_postal", "cod_postal");
            ViewBag.Morada = new SelectList(CombosHelper.GetCodigosPostais().OrderBy(m => m.morada_completa), "morada_completa", "cod_completo");

            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCliente,PrimeiroNome,Apelido,Morada,CodPostal,Telefone,Email")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Clientes/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.CodPostal = new SelectList(CombosHelper.GetCodigosPostais().OrderBy(m => m.cod_postal), "cod_postal", "cod_postal", cliente.CodPostal);
            ViewBag.Morada = new SelectList(CombosHelper.GetCodigosPostais().OrderBy(m => m.morada_completa), "morada_completa", "cod_completo", cliente.Morada);

            return View(cliente);

        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdCliente,PrimeiroNome,Apelido,Morada,CodPostal,Telefone,Email")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
