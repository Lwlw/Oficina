﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            OficinaContext db = new OficinaContext();

            ViewBag.Marco2018 = db.Reparacaos.Where(r => r.DataInicio.Month == 3 && r.DataInicio.Year == 2018).Count();
            ViewBag.Abril2018 = db.Reparacaos.Where(r => r.DataInicio.Month == 4 && r.DataInicio.Year == 2018).Count();
            ViewBag.Maio2018 = db.Reparacaos.Where(r => r.DataInicio.Month == 5 && r.DataInicio.Year == 2018).Count();
            ViewBag.Junho2018 = db.Reparacaos.Where(r => r.DataInicio.Month == 6 && r.DataInicio.Year == 2018).Count();
            ViewBag.Julho2018 = db.Reparacaos.Where(r => r.DataInicio.Month == 7 && r.DataInicio.Year == 2018).Count();

            ViewBag.Teste = db.Reparacaos.Where(r => r.DataInicio.Month == 5 && r.DataInicio.Year == 2018);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Oficinas MVC - CET30 TPSI CINEL";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}