﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Oficina.Helpers;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class ViaturasController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Viaturas
        //[Authorize(Roles = "View")]
        public ActionResult Index()
        {
            var viaturas = db.Viaturas.Include(v => v.Cliente);
            ViewBag.Contagem = viaturas.Count();
            return View(viaturas.ToList());
        }

        // GET: Viaturas/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Viatura viatura = db.Viaturas.Find(id);
            if (viatura == null)
            {
                return HttpNotFound();
            }
            return View(viatura);
        }

        // GET: Viaturas/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {

            ViewBag.Marca = new SelectList(CombosHelper.GetCarros().DistinctBy(x => x.Marca), "Marca", "Marca");
            ViewBag.Modelo = new SelectList(CombosHelper.GetCarros(), "Modelo", "MarcaModelo");


            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nome");

            return View();
        }

        // POST: Viaturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdViatura,Matricula,Marca,Modelo,Cor,Ano,IdCliente")] Viatura viatura)
        {
            if (ModelState.IsValid)
            {
                db.Viaturas.Add(viatura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nome", viatura.IdCliente);
            return View(viatura);
        }

        // GET: Viaturas/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Viatura viatura = db.Viaturas.Find(id);
            if (viatura == null)
            {
                return HttpNotFound();
            }
            ViewBag.Marca = new SelectList(CombosHelper.GetCarros().DistinctBy(x => x.Marca), "Marca", "Marca", viatura.Marca);
            ViewBag.Modelo = new SelectList(CombosHelper.GetCarros(), "Modelo", "MarcaModelo", viatura.Modelo);
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nome", viatura.IdCliente);
            return View(viatura);
        }

        // POST: Viaturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdViatura,Matricula,Marca,Modelo,Cor,Ano,IdCliente")] Viatura viatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nome", viatura.IdCliente);
            return View(viatura);
        }

        // GET: Viaturas/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Viatura viatura = db.Viaturas.Find(id);
            if (viatura == null)
            {
                return HttpNotFound();
            }
            return View(viatura);
        }

        // POST: Viaturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Viatura viatura = db.Viaturas.Find(id);
            db.Viaturas.Remove(viatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
