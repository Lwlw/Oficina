﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Oficina.Helpers;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class FaturasController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Faturas
        //[Authorize(Roles = "View")]
        public ActionResult Index()
        {
            var faturas = db.Faturas.Include(f => f.Reparacao);
            ViewBag.Contagem = faturas.Count();

            return View(faturas.ToList());
        }

        // GET: Faturas/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }

            //3 x viewbags?
            ViewBag.Detalhes = db.ReparacaoDetalhes.Where(x => x.IdReparacao == fatura.IdReparacao).ToList();
            
            var reparacao = db.Reparacaos.FirstOrDefault(r => r.IdReparacao == fatura.IdReparacao);
            var horas = Reparacao.GetDiasUteis(reparacao.DataInicio, (DateTime)reparacao.DataFim) * 8;
            var minutos = Convert.ToInt32(Math.Floor((horas - Math.Floor(horas)) * 60)).ToString("00");
            ViewBag.Horas = $"{Math.Floor(horas)}h{minutos}m";
            var mecanico = db.Mecanicoes.Find(reparacao.IdMecanico);
            ViewBag.MaoObraIva = mecanico.CustoHora + 0.23M * mecanico.CustoHora;

            return View(fatura);
        }

        // GET: Faturas/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao");
            return View();
        }

        // POST: Faturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Numero,IdCliente,TotalMaoObra,TotalConsumiveis,IdReparacao,DataEmissao")] Fatura fatura)
        {
            if (ModelState.IsValid)
            {
                db.Faturas.Add(fatura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", fatura.IdReparacao);
            return View(fatura);
        }

        // GET: Faturas/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", fatura.IdReparacao);
            return View(fatura);
        }

        // POST: Faturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Numero,IdCliente,TotalMaoObra,TotalConsumiveis,IdReparacao,DataEmissao")] Fatura fatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", fatura.IdReparacao);
            return View(fatura);
        }

        // GET: Faturas/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }
            return View(fatura);
        }

        // POST: Faturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Fatura fatura = db.Faturas.Find(id);
            db.Faturas.Remove(fatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
