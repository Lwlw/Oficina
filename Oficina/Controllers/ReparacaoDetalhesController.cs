﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class ReparacaoDetalhesController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: ReparacaoDetalhes
        //[Authorize(Roles = "View")]
        public ActionResult Index()
        {
            var reparacaoDetalhes = db.ReparacaoDetalhes.Include(r => r.Peca).Include(r => r.Reparacao);
            return View(reparacaoDetalhes.ToList());
        }

        // GET: ReparacaoDetalhes/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReparacaoDetalhe reparacaoDetalhe = db.ReparacaoDetalhes.Find(id);
            if (reparacaoDetalhe == null)
            {
                return HttpNotFound();
            }
            return View(reparacaoDetalhe);
        }

        // GET: ReparacaoDetalhes/Create
        public ActionResult Create()
        {
            ViewBag.IdPeca = new SelectList(db.Pecas, "IdPeca", "Descricao");
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao");
            return View();
        }

        // POST: ReparacaoDetalhes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdReparacaoDetalhe,IdReparacao,IdPeca,Quantidade")] ReparacaoDetalhe reparacaoDetalhe)
        {
            if (ModelState.IsValid)
            {
                db.ReparacaoDetalhes.Add(reparacaoDetalhe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdPeca = new SelectList(db.Pecas, "IdPeca", "Descricao", reparacaoDetalhe.IdPeca);
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", reparacaoDetalhe.IdReparacao);
            return View(reparacaoDetalhe);
        }

        // GET: ReparacaoDetalhes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReparacaoDetalhe reparacaoDetalhe = db.ReparacaoDetalhes.Find(id);
            if (reparacaoDetalhe == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdPeca = new SelectList(db.Pecas, "IdPeca", "Descricao", reparacaoDetalhe.IdPeca);
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", reparacaoDetalhe.IdReparacao);
            return View(reparacaoDetalhe);
        }

        // POST: ReparacaoDetalhes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdReparacaoDetalhe,IdReparacao,IdPeca,Quantidade")] ReparacaoDetalhe reparacaoDetalhe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reparacaoDetalhe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdPeca = new SelectList(db.Pecas, "IdPeca", "Descricao", reparacaoDetalhe.IdPeca);
            ViewBag.IdReparacao = new SelectList(db.Reparacaos, "IdReparacao", "Descricao", reparacaoDetalhe.IdReparacao);
            return View(reparacaoDetalhe);
        }

        // GET: ReparacaoDetalhes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReparacaoDetalhe reparacaoDetalhe = db.ReparacaoDetalhes.Find(id);
            if (reparacaoDetalhe == null)
            {
                return HttpNotFound();
            }
            return View(reparacaoDetalhe);
        }

        // POST: ReparacaoDetalhes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReparacaoDetalhe reparacaoDetalhe = db.ReparacaoDetalhes.Find(id);
            db.ReparacaoDetalhes.Remove(reparacaoDetalhe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
