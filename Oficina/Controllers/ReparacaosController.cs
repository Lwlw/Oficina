﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Oficina.Models;

namespace Oficina.Controllers
{
    public class ReparacaosController : Controller
    {
        private OficinaContext db = new OficinaContext();

        // GET: Reparacaos
        //[Authorize(Roles = "View")]
        public ActionResult Index(string cliente, string search, string ordenar)
        {
            
            var reparacaos = db.Reparacaos.Include(r => r.Mecanico).Include(r => r.Viatura);
            
            var reps = reparacaos;
            //var nomeClientes = reparacaos.SelectMany(r => db.Viaturas.Where(v => r.IdViatura == v.IdViatura));
            //var nomeClientes = reparacaos.Any(r => r.Cliente.Contains(search));
            var clientes = db.Clientes.Where(c => (c.PrimeiroNome + " " + c.Apelido).Contains(search));
            var reparacaosclientes = clientes.SelectMany(c => reps.Where(r => r.Viatura.IdCliente == c.IdCliente));



            if (!String.IsNullOrEmpty(search))
            {
                reparacaos = reparacaos.Where(r => r.Descricao.Contains(search) ||
                                                   r.Mecanico.Nome.Contains(search) ||
                                                   r.Viatura.Matricula.Contains(search) ||
                                                   r.Viatura.Marca.Contains(search) ||
                                                   r.Viatura.Modelo.Contains(search) ||
                                                   r.Status.ToString().Contains(search));
                ViewBag.Search = search;
            }


            var reparacaoz = reparacaosclientes.Union(reparacaos);


            var clients = reparacaoz.OrderBy(r => r.Viatura.Cliente.PrimeiroNome + " " + r.Viatura.Cliente.Apelido).
                Select(r => r.Viatura.Cliente.PrimeiroNome + " " + r.Viatura.Cliente.Apelido).Distinct();

            if (!String.IsNullOrEmpty(cliente))
            {
                reparacaoz = reparacaoz.Where(r =>
                    r.Viatura.Cliente.PrimeiroNome + " " + r.Viatura.Cliente.Apelido == cliente);
            }

            ViewBag.Cliente = new SelectList(clients);

            ViewBag.Contagem = reparacaoz.Count();

            if (!string.IsNullOrEmpty(ordenar))
            {
                switch (ordenar)
                {
                    case "ascendente":
                        reparacaoz = reparacaoz.OrderBy(r => r.DataInicio);
                        break;
                    case "descendente":
                        reparacaoz = reparacaoz.OrderByDescending(r => r.DataInicio);
                        break;
                }
            }
            List<string> ordem = new List<string>(new[] { "ascendente", "descendente" });
            //var ordenarData = ordem.Where(x => !string.IsNullOrEmpty(x));
            ViewBag.Ordenar = new SelectList(ordem);

            return View(reparacaoz.ToList());
        }

        // GET: Reparacaos/Details/5
        [Authorize(Roles = "View")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Reparacao reparacao = db.Reparacaos.Find(id);

            if (reparacao == null)
            {
                return HttpNotFound();
            }
            ViewBag.Detalhes = db.ReparacaoDetalhes.Where(x => x.IdReparacao == id).ToList();

            return View(reparacao);
        }

        // GET: Reparacaos/Create
        [Authorize(Roles = "Create")]
        public ActionResult Create()
        {
            ViewBag.IdMecanico = new SelectList(db.Mecanicoes, "IdMecanico", "Nome");
            ViewBag.IdViatura = new SelectList(db.Viaturas, "IdViatura", "Matricula");
            return View();
        }

        // POST: Reparacaos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdReparacao,Descricao,IdViatura,IdMecanico,DataInicio,DataFim,Status")] Reparacao reparacao)
        {
            if (ModelState.IsValid)
            {
                db.Reparacaos.Add(reparacao);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.IdMecanico = new SelectList(db.Mecanicoes, "IdMecanico", "Nome", reparacao.IdMecanico);
            ViewBag.IdViatura = new SelectList(db.Viaturas, "IdViatura", "Matricula", reparacao.IdViatura);
            return View(reparacao);
        }

        // GET: Reparacaos/Edit/5
        [Authorize(Roles = "Edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reparacao reparacao = db.Reparacaos.Find(id);
            if (reparacao == null)
            {
                return HttpNotFound();
            }

            Session["inicial"] = reparacao.DataInicio;
            ViewBag.Inic = reparacao.DataInicio;

            ViewBag.IdMecanico = new SelectList(db.Mecanicoes, "IdMecanico", "Nome", reparacao.IdMecanico);
            ViewBag.IdViatura = new SelectList(db.Viaturas, "IdViatura", "Matricula", reparacao.IdViatura);
            return View(reparacao);

        }


        // POST: Reparacaos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdReparacao,Descricao,IdViatura,IdMecanico,DataInicio,DataFim,Status")] Reparacao reparacao)
        {
            reparacao.DataInicio = Convert.ToDateTime(Session["inicial"]);
            ViewBag.Inic = reparacao.DataInicio;

            if (reparacao.DataFim == null || reparacao.DataFim < reparacao.DataInicio)
            {
                ViewBag.Error = "Insira uma data de fecho da reparação não inferior à inicial";

                ViewBag.IdMecanico = new SelectList(db.Mecanicoes, "IdMecanico", "Nome", reparacao.IdMecanico);
                ViewBag.IdViatura = new SelectList(db.Viaturas, "IdViatura", "Matricula", reparacao.IdViatura);

                return View(reparacao);

            }

            var mecanico = db.Mecanicoes.First(x => x.IdMecanico == reparacao.IdMecanico);
            //var totalMaoObra = ((DateTime)reparacao.DataFim).Subtract(reparacao.DataInicio).Days * 8 *
            //                   mecanico.CustoHora;
            var datafinal = (DateTime)reparacao.DataFim;
            var totalMaoObra = (decimal)Reparacao.GetDiasUteis(reparacao.DataInicio, datafinal) * 8 *
                               mecanico.CustoHora;

            if (ModelState.IsValid)
            {
                db.Entry(reparacao).State = EntityState.Modified;
                db.SaveChanges();

                
                Fatura fatura = new Fatura
                {
                    IdReparacao = reparacao.IdReparacao,
                    DataEmissao = Convert.ToDateTime(reparacao.DataFim),
                    IdCliente = db.Viaturas.FirstOrDefault(x => x.IdViatura == reparacao.IdViatura).IdCliente,
                    //TotalMaoObra = Fatura.GetTotalMaoObra(reparacao.IdReparacao),
                    TotalMaoObra = totalMaoObra + 0.23M * totalMaoObra,
                    TotalConsumiveis = Fatura.GetTotalConsumiveis(reparacao.IdReparacao) 
                };

                if (reparacao.Status == Status.Finalizada)
                {
                    db.Faturas.Add(fatura);
                    db.SaveChanges();
                }


                //return RedirectToAction("Index");            

                ViewBag.Message = $"A reparação nº. {reparacao.IdReparacao} foi fechada com sucesso!";

                ViewBag.IdMecanico = new SelectList(db.Mecanicoes, "IdMecanico", "Nome", reparacao.IdMecanico);
                ViewBag.IdViatura = new SelectList(db.Viaturas, "IdViatura", "Matricula", reparacao.IdViatura);
                return View(reparacao);

            }


            return View(reparacao);

        }

        // GET: Reparacaos/Delete/5
        [Authorize(Roles = "Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reparacao reparacao = db.Reparacaos.Find(id);
            if (reparacao == null)
            {
                return HttpNotFound();
            }
            return View(reparacao);
        }

        // POST: Reparacaos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reparacao reparacao = db.Reparacaos.Find(id);
            try
            {

                db.Reparacaos.Remove(reparacao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                
                ViewBag.Error = "Primeiro tem de apagar os registos (detalhes e fatura) associados à reparação";
                return View();
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }


}
