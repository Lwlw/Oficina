﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Oficina.Helpers
{
    public class FilesHelper
    {
        public static bool UploadImagem (HttpPostedFileBase ficheiro, string pasta, string nome)
        {
            if (ficheiro == null || string.IsNullOrEmpty(pasta) || string.IsNullOrEmpty(nome))
            {
                return false;
            }

            try
            {
                string path = string.Empty;

                if (ficheiro != null)
                {
                    path = Path.Combine(HttpContext.Current.Server.MapPath(pasta), nome);
                    ficheiro.SaveAs(path);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        ficheiro.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}