﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using Oficina.Models;
using Oficina.ViewModels;

namespace Oficina.Helpers
{
    public class CombosHelper : IDisposable
    {

        private static OficinaContext db = new OficinaContext();
        private static ApplicationDbContext da = new ApplicationDbContext();

        public static List<Cliente> GetClientes()
        {
            var clientes = db.Clientes.ToList();
            clientes.Add(new Cliente
            {
                IdCliente = 0,
            });

            return clientes.OrderBy(d => d.Nome).ToList();
        }

        public static List<Mecanico> GetMecanicos()
        {
            var mecanicos = db.Mecanicoes.ToList();
            mecanicos.Add(new Mecanico
            {
                IdMecanico = 0,
            });

            return mecanicos.OrderBy(d => d.Nome).ToList();
        }

        public static List<Viatura> GetViaturas()
        {
            var viaturas = db.Viaturas.ToList();
            viaturas.Add(new Viatura
            {
                IdViatura = 0,
            });

            return viaturas.OrderBy(d => d.Matricula).ToList();
        }

        public static List<Peca> GetPecas()
        {
            var pecas = db.Pecas.ToList();
            pecas.Add(new Peca
            {
                IdPeca = 0,
                Descricao = " --------Selecione uma Peça-------- "
            });

            return pecas.OrderBy(p => p.Descricao).ToList();
        }


        public static List<Carros> GetCarros()
        {
            List<Carros> carros;
            List<Carros> modelos = new List<Carros>();

            using (StreamReader r = new StreamReader(HostingEnvironment.MapPath("~/Content/viaturas.json")))
            {
                string json = r.ReadToEnd();
                carros = JsonConvert.DeserializeObject<List<Carros>>(json);

                carros = carros.OrderBy(x => x.MarcaModelo).ToList();

                foreach (var items in carros)
                {
                    foreach (var item in items.Modelos)
                    {
                        modelos.Add(new Carros
                        {
                            Marca = items.Marca,
                            Modelo = item,
                           
                        });

                    }
                }
                return modelos;
            }
        }

 


        public static List<CodigoPostal> GetCodigosPostais()
        {

            List<CodigoPostal> cp;

            //using (var webClient = new System.Net.WebClient())
            //{

            //    var json = webClient.DownloadString("http://centraldedados.pt/codigos_postais.json");
            //    cp = JsonConvert.DeserializeObject<List<CodigoPostal>>(json);
            //}

            using (StreamReader r = new StreamReader(HostingEnvironment.MapPath("~/Content/codpost.json")))
            {
                string json = r.ReadToEnd();
                cp = JsonConvert.DeserializeObject<List<CodigoPostal>>(json);

                //cp = cp.OrderBy(x => x.cod_completo).ToList();
            }


            var cp2 = cp.Where(c => !string.IsNullOrWhiteSpace(c.morada_completa)).DistinctBy(x => x.morada_completa).ToList();
            return cp2;

        }

        public static List<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(da));
            var list = roleManager.Roles.ToList();
            list.Add(new IdentityRole { Id = "", Name = "[Selecione uma permissão...]" });

            return list.OrderBy(r => r.Name).ToList();

        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}