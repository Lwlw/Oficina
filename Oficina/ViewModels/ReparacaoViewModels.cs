﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oficina.Models;

namespace Oficina.ViewModels
{
    public class ReparacaoViewModels
    {
        public Viatura Viatura { get; set; }

        [DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "dd/mm/yyyy hh:mm", ApplyFormatInEditMode = true)]
        [Display(Name = "Data Início")]
        [Required(ErrorMessage = "O campo {0} é de preenchimento obrigatório")]
        public DateTime Data { get; set; }

        public Mecanico Mecanico { get; set; }

        public PecaReparacao Peca { get; set; }

        public List<PecaReparacao> Pecas { get; set; }
    }
}