﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oficina.ViewModels
{
    public class UserViewModels
    {
        public string UserID { get; set; }

        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public RoleViewModels Role { get; set; }

        public List<RoleViewModels> Roles { get; set; }
    }
}