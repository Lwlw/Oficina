﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class PecaReparacao:Peca
    {

        [Required(ErrorMessage = "Deve inserir uma {0}")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Deve inserir uma {0} superior a zero")]
        public int Quantidade { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Valor a pagar")]
        public decimal Valor { get { return Preco * (decimal)Quantidade; } }
    }
}