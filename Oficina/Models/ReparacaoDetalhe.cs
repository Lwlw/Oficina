﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class ReparacaoDetalhe
    {
        private OficinaContext db = new OficinaContext();

        [Key]
        public int IdReparacaoDetalhe { get; set; }

        [Required]
        public int IdReparacao { get; set; }

        [Required(ErrorMessage = "Deve selecionar uma {}")]
        [Display(Name="Peça")]
        public int IdPeca { get; set; }

        [Required(ErrorMessage = "Deve inserir uma {0}")]
        public int Quantidade { get; set; }

        [NotMapped]
        public decimal ValorTotal
        {
            get
            {
                var peca = db.Pecas.First(p => p.IdPeca == IdPeca);
                return Quantidade * peca.Preco;
            }
        }




        public virtual Reparacao Reparacao { get; set; }

        public virtual Peca Peca { get; set; }
        
    }
    
}