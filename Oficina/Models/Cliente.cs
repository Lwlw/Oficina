﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class Cliente
    {
        [Key]
        public int IdCliente { get; set; }

        [StringLength(30, ErrorMessage = "O {0} deve ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage = "O campo {0} não pode ficar em branco")]
        [Display(Name = "Primeiro Nome")]
        [RegularExpression(@"^[A-ZÀ-Ÿ][A-zÀ-ÿ''-'\s]+$", ErrorMessage = "Capitalize e siga as convenções habituais para {0}")]
        public string PrimeiroNome { get; set; }

        [StringLength(30, ErrorMessage = "O {0} deve ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage = "Tem que inserir {0}")]
        [Display(Name = "Apelido")]
        [RegularExpression(@"^([A-zÀ-ÿ']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ']+$", ErrorMessage = "Capitalize e siga as convenções habituais para {0}")]
        public string Apelido { get; set; }

        [Display(Name = "Nome")]
        [NotMapped]
        public string Nome { get { return $"{PrimeiroNome} {Apelido}"; } }
        //public override string ToString() => $"{PrimeiroNome} {Apelido}".Trim();
        //public void Nome() => Console.WriteLine(ToString());

        [Required]
        public string Morada { get; set; }

        [Required(ErrorMessage = "Tem que inserir um {0}")]
        [Display(Name = "Código Postal")]
        [RegularExpression(@"\d{4}([\-]\d{3})?", ErrorMessage = "Formatos válidos: 9999 ou 9999-999")]
        public string CodPostal { get; set; }

        [Range(0, Int64.MaxValue, ErrorMessage = "O campo {0} apenas admite digitos")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(9, ErrorMessage = "O campo {0} deve conter {1} digitos", MinimumLength = 9)]
        [Required(ErrorMessage = "Campo {0} obrigatório")]
        [Display(Name = "Telefone")]
        public string Telefone { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Insira um {0} válido")]
        public string Email { get; set; }



        public virtual ICollection<Viatura> Viaturas { get; set; }

    }



}