﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class OficinaContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public OficinaContext() : base("name=OficinaContext")
        {
        }

        //may blow vid 18
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<Oficina.Models.Cliente> Clientes { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.Viatura> Viaturas { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.Reparacao> Reparacaos { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.Mecanico> Mecanicoes { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.Fatura> Faturas { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.Peca> Pecas { get; set; }

        public System.Data.Entity.DbSet<Oficina.Models.ReparacaoDetalhe> ReparacaoDetalhes { get; set; }
    }
}
