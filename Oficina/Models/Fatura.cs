﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oficina.Models
{

    public class Fatura
    {

        [Key]
        [Display(Name = "Número Fatura")]
        public int Numero { get; set; }

        [Display(Name = "Cliente")]
        public int IdCliente { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Mão-de-obra c/ IVA")]
        public decimal TotalMaoObra { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Consumíveis c/ IVA")]
        public decimal TotalConsumiveis { get; set; }

        //[ForeignKey("Reparacao")]
        [Display(Name = "Reparação")]
        public int IdReparacao { get; set; }

        [Display(Name = "Data de emissão")]
        public DateTime DataEmissao { get; set; }



        public virtual Reparacao Reparacao { get; set; }



        //public static decimal GetTotalMaoObra(int IdRepara)
        //{
        //    OficinaContext db = new OficinaContext();

        //    decimal totl = 0.0M;
        //    Reparacao reparacao = db.Reparacaos.Find(IdRepara);
        //    var custoMec = db.Mecanicoes.FirstOrDefault(m =>
        //        m.IdMecanico == reparacao.IdMecanico).CustoHora;

        //    var terminus = (DateTime)reparacao.DataFim;
        //    var horas = terminus.Subtract(reparacao.DataInicio).Hours;

        //    totl = custoMec * horas;

        //    return totl;
        //}

        public static decimal GetTotalConsumiveis(int IdRepara)
        {
            OficinaContext db = new OficinaContext();

            decimal totl = 0.0M;
            var consumiveis = db.ReparacaoDetalhes.Where(r => r.IdReparacao == IdRepara);

            if (consumiveis.Count() != 0)
                foreach (var peca in consumiveis)
                {
                    totl += peca.ValorTotal;
                }

            return totl;

        }
    }
}