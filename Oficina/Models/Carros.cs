﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    [NotMapped]
    public class Carros
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public List<string> Modelos { get; set; }

        public string MarcaModelo
        {
            get { return $"{Modelo} |{Marca}|"; }
        }
    }
}