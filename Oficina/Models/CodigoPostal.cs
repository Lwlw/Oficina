﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    [NotMapped]
    public class CodigoPostal
    {

        //public string cod_distrito { get; set; }
        //public string cod_concelho { get; set; }
        //public string cod_localidade { get; set; }
        //public string nome_localidade { get; set; }
        //public string cod_arteria { get; set; }
        public string tipo_arteria { get; set; }
        public string prep1 { get; set; }
        public string titulo_arteria { get; set; }
        public string prep2 { get; set; }
        public string desig_arteria { get; set; }
        public string local_arteria { get; set; }
        //public string troco { get; set; }
        //public string porta { get; set; }
        //public string cliente { get; set; }
        public string num_cod_postal { get; set; }
        public string ext_cod_postal { get; set; }
        public string desig_postal { get; set; }

        public string morada_completa
        {
            get
            {
                string morada = "";
                morada += tipo_arteria + " "; morada += prep1.Length > 0 ? prep1 + " " : "";
                morada += titulo_arteria.Length > 0 ? titulo_arteria + " " : "";
                morada += prep2.Length > 0 ? prep2 + " " : "";
                morada += desig_arteria.Length > 0 ? desig_arteria + " " : "";
                morada += local_arteria.Length > 0 ? local_arteria + " " : "";
                morada = morada.Trim();
                //morada = morada.Replace("\0", string.Empty);
                return morada;
            }
        }

        public string cod_postal
        {
            get
            {
                if (num_cod_postal != "")
                    return num_cod_postal + "-" + ext_cod_postal;
                else
                    return "";
            }
        }

        public string cod_completo
        {
            get
            {
                if (morada_completa != "")
                    return $"{morada_completa} |{cod_postal}|";
                else
                    return "";
            }
        }

    }
}