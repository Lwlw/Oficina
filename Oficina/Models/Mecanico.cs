﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace Oficina.Models
{
    public class Mecanico
    {
        [Key]
        public int IdMecanico { get; set; }

        [Required(ErrorMessage = "Deve inserir o {0}")]
        [Display(Name = "Mecânico")]
        [RegularExpression(@"^[A-ZÀ-Ÿ][A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ']+$", ErrorMessage = "Capitalize e siga as convenções habituais para Nome + Apelido")]
        public string Nome { get; set; }

        [NotMapped]
        public string NomeMecanico
        {
            get
            {
                if (IdMecanico == 0)
                    return " --------Selecione um Mecânico-------- ";

                //var cliente = CombosHelper.GetClientes().
                //    First(c => c.IdCliente == IdCliente);
                return Nome;
            }
        }

        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "dd/mm/yyyy", ApplyFormatInEditMode = true)]
        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "Tem de inserir uma {0}")]
        public DateTime DataNascimento { get; set; }

        [NotMapped]
        public int Idade
        {
            get
            {
                var idad = DateTime.Now.Year - DataNascimento.Year;
                if (DataNascimento > DateTime.Now.AddYears(-idad))
                {
                    idad--;
                }
                return idad;
            }
        }

        [DataType(DataType.PhoneNumber)]
        [StringLength(9, ErrorMessage = "O campo {0} deve conter {1} carateres", MinimumLength = 9)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        [Display(Name = "Telefone")]
        public string Telefone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Custo/Hora")]
        [Required(ErrorMessage = "Deve inserir o {0}")]
        public decimal CustoHora { get; set; }



        public virtual ICollection<Reparacao> Reparacaos { get; set; }

    }
}