﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oficina.Helpers;

namespace Oficina.Models
{
    public class Viatura
    {

        [Key]
        public int IdViatura { get; set; }

        [Required(ErrorMessage = "O campo {0} é de preenchimento obrigatório")]
        [RegularExpression(@"((?:((?![KWY])[A-Z]){2}-\d{2}-\d{2})|(?:\d{2}-((?![KWY])[A-Z]){2}-\d{2})|(?:\d{2}-\d{2}-((?![KWY])[A-Z]){2}))",
            ErrorMessage = "Formato: AA-00-00, 00-AA-00 ou 00-00-AA " +
                           "(letras maiúsculas excepto 'K', 'W' e 'Y')")]
        public string Matricula { get; set; }

        [Required(ErrorMessage = "Tem que inserir a {0}")]
        public string Marca { get; set; }

        [Required(ErrorMessage = "Tem que inserir o {0}")]
        public string Modelo { get; set; }

        [NotMapped]
        public List<string> Modelos { get; set; }

        public string Cor { get; set; }

        [Required(ErrorMessage = "Tem que inserir o {0}")]
        [Range(1950, 2018, ErrorMessage = "Valor para {0} entre {1} e {2}")]
        public int Ano { get; set; }

        [Required]
        public int IdCliente { get; set; }

        [NotMapped]
        public string ClienteNome
        {
            get
            {
                if (IdViatura == 0)
                    return " --------Selecione um Cliente-------- ";

                //var cliente = CombosHelper.GetClientes().
                //    First(c => c.IdCliente == IdCliente);
                return Cliente.Nome;
            }
        }

        //public virtual ICollection<ClienteViatura> ClienteViaturas { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual ICollection<Reparacao> Reparacaos { get; set; }

    }
}