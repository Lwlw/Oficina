﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using Oficina.Models;

namespace Oficina.Models
{

    public class Reparacao
    {

        OficinaContext db = new OficinaContext();

        [Key]
        public int IdReparacao { get; set; }

        [Display(Name = "Descricao")]
        [StringLength(35, ErrorMessage = "A {0} deve ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage = "Deve inserir uma {0}")]
        public string Descricao { get; set; }

        [Display(Name="Viatura")]
        public int IdViatura { get; set; }

        [NotMapped]
        public string Cliente
        {
            get
            {
                var cliente = db.Clientes.
                    First(c => c.IdCliente == db.Viaturas.
                    FirstOrDefault(v => v.IdViatura == IdViatura).IdCliente);
                return cliente.Nome;
            }
        }

        [DisplayName("Mecânico")]
        public int IdMecanico { get; set; }

        //[NotMapped]
        //public string MecanicoNome
        //{
        //    get
        //    {
        //        //var mecanico = db.Mecanicoes.FirstOrDefault(m => m.IdMecanico == IdMecanico);
        //        return Mecanico.Nome; //desnecessário
        //    }
        //}

        [DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "dd/mm/yyyy hh:mm", ApplyFormatInEditMode = true)]
        [Display(Name = "Data Início")]
        [Required(ErrorMessage = "O campo {0} é de preenchimento obrigatório")]
        public DateTime DataInicio { get; set; }

        [DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "dd/mm/yyyy hh:mm", ApplyFormatInEditMode = true)]
        [Display(Name = "Data Fim")]
        public DateTime? DataFim { get; set; }

        public Status Status { get; set; }





        public virtual Viatura Viatura { get; set; }

        public virtual Mecanico Mecanico { get; set; }

        public virtual ICollection<Fatura> Faturas { get; set; }

        public virtual ICollection<ReparacaoDetalhe> ReparacaoDetalhes { get; set; }




        public static double GetDiasUteis(DateTime dataInicio, DateTime dataFim)
        {
            var diasUteis =
                1 + ((dataFim - dataInicio).TotalDays * 5 -
                     (dataInicio.DayOfWeek - dataFim.DayOfWeek) * 2) / 7;

            if (dataFim.DayOfWeek == DayOfWeek.Saturday)
                diasUteis--;
            if (dataInicio.DayOfWeek == DayOfWeek.Sunday)
                diasUteis--;

            return diasUteis;
        }
    }

    public enum Status { Activa, Finalizada }
}