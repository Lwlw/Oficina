﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class Peca
    {
        [Key]
        public int IdPeca { get; set; }

        [Display(Name = "Descrição")]
        [StringLength(30, ErrorMessage = "A {0} devera ter entre {2} e {1} carateres", MinimumLength = 3)]
        [Required(ErrorMessage = "Deve inserir uma {0}")]
        public string Descricao { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Preço (c/ IVA)")]
        [Required(ErrorMessage = "Deve inserir um {0}")]
        public decimal Preco { get; set; }
        
        public string Imagem { get; set; }

        [NotMapped]
        public HttpPostedFileBase PecaImagemURL { get; set; }




        public virtual ICollection<ReparacaoDetalhe> ReparacaoDetalhes { get; set; }
    }


}