﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oficina.Models
{
    public class ClientePostal
    {
        public Cliente Cliente { get; set; }

        public CodigoPostal CodigoPostal { get; set; }
    }
}